import EditorMgr from "./editor.mjs"
import CanvasMgr from "./renderer.mjs"

export class App {
  #editor = null;
  #canvas = null;
  #exit = document.createElement("div");
  constructor(img, canvas, editor) {
    canvas.appendChild(this.#exit);
    const sign = document.createElement("div");
    sign.classList.add("sign_cross");
    this.#exit.appendChild(sign);
    this.#exit.id = "exit";
    chrome.storage.local.get("sources").then(data => {
      debugger;
      this.#editor = new EditorMgr(editor);
      this.#canvas = new CanvasMgr(canvas, img);
      this.#editor.import(data.sources);
      this.#registerCallback();
    });
  }
  #registerCallback() {
    this.#exit.addEventListener("mousedown", () => {
      const data = this.#editor.export();
      chrome.storage.local.set({sources: data}).then(() => window.close());
    });
  }
};