"use strict";

function createContextMenu() {
  chrome.contextMenus.create({
    contexts: ["image"],
    id: "menu",
    title: "launch pict-filt"
  });
}

async function initSourceData() {
  await chrome.storage.local.clear();
  fetch("init.pictfilt")
    .then(request => request.json())
    .then(data => chrome.storage.local.set(data));
}

chrome.runtime.onInstalled.addListener(() => {
  createContextMenu();
  initSourceData();
});

chrome.contextMenus.onClicked.addListener(item => {
  chrome.tabs.create({ url: item.srcUrl }).then(tab => {
    const on_complete = (id, info) => {
      if (id === tab.id && info.status === "complete") {
        chrome.tabs.onUpdated.removeListener(on_complete);
        chrome.tabs.sendMessage(tab.id, { call: "init_app" });
      }
    };
    chrome.tabs.onUpdated.addListener(on_complete);
  });
});