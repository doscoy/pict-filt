"use strict";

let app = null;

function removeStyle() {
  document.getElementsByTagName("html").item(0).removeAttribute("style");
  document.body.removeAttribute("style");
}

function setId2Image() {
  const img = document.getElementsByTagName("img").item(0);
  img.id = "pict";
  img.display = "none";
  return img;
}

function createContainers() {
  const mask = document.createElement("div");
  mask.id = "mask";
  document.body.appendChild(mask);
  const maker = (id) => {
    const elm = document.createElement("div");
    elm.id = id;
    mask.appendChild(elm);
    return elm;
  }
  return [maker("canvas_container"), maker("editor_container")];
}

function loadCSS() {
  const css = document.createElement("link");
  document.head.appendChild(css);
  css.rel = "stylesheet";
  css.type = "text/css";
  css.href = chrome.runtime.getURL("/style.css");
}

function loadApp(img, canvas, editor) {
  import("/app.mjs").then(module => {
    app = new module.App(img, canvas, editor);
  });
}

function initApp() {
  removeStyle();
  loadCSS();
  const img = setId2Image();
  const containers = createContainers();
  if (document.readyState !== "loading") {
    loadApp(img, containers[0], containers[1]);
  } else {
    const delay = e => {
      const state = e.target.readyState;
      if (state === "interactive" || state === "complete") {
        loadApp(img, containers[0], containers[1]);
        document.removeEventListener("readystatechange", delay);
      }
    };
    document.addEventListener("readystatechange", delay);
  }
}

chrome.runtime.onMessage.addListener(request => {
  switch (request.call) {
    case "init_app":
      initApp();
      break;
    default:
      break;
  }
});