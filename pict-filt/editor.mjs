import List from "./list.mjs"
import * as MyEvent from "./event.mjs"

class ElementBase {
  #parent;
  #element;
  constructor(parent, container) {
    this.#parent = parent;
    if (container !== undefined) {
      this.#element = container;
    } else {
      this.#element = document.createElement("div");
    }
  }
  get parent() { return this.#parent; }
  get element() { return this.#element; }
  get width() { return this.#element.clientWidth; }
  get height() { return this.#element.clientHeight; }
  set parent(obj) { this.#parent = obj; }
  set width(number) { this.#element.style.width = number + "px"; }
  set height(number) { this.#element.style.height = number + "px"; }
}

class ForcedCursor {
  #style = document.createElement("style");
  constructor(cursor) {
    this.#style.textContent = `*{cursor:${cursor}!important;}`;
  }
  on() {
    document.head.appendChild(this.#style);
  }
  off() {
    document.head.removeChild(this.#style);
  }
}

export default class EditorMgr extends ElementBase {
  #handle = document.createElement("div");
  #editor_area = new EditorArea(this);
  #callback = {
    down: () => this.#handleDown(),
    move: e => this.#handleMove(e),
    up: () => this.#handleUp()
  }
  #forced_cursor;
  constructor(container) {
    super(null, container);
    this.element.appendChild(this.#handle);
    this.element.appendChild(this.#editor_area.element);
    this.#handle.id = "handle";
    const cursor = window.getComputedStyle(this.#handle).getPropertyValue("cursor");
    this.#forced_cursor = new ForcedCursor(cursor);
    this.#registerCallback();
  }
  #registerCallback() {
    this.#handle.addEventListener("mousedown", this.#callback.down);
    MyEvent.Mul.add("change_source", () => this.#callDraw());
  }
  #handleDown() {
    window.addEventListener("mousemove", this.#callback.move);
    window.addEventListener("mouseup", this.#callback.up);
    this.#forced_cursor.on();
  }
  #handleMove(e) {
    const w = document.body.clientWidth - this.#handle.clientWidth / 2 - e.pageX;
    const open_thr = 30;
    const upper = document.body.clientWidth - this.#handle.clientWidth - open_thr;
    this.#editor_area.width = Math.min(w, upper);
  }
  #handleUp() {
    window.removeEventListener("mousemove", this.#callback.move);
    window.removeEventListener("mouseup", this.#callback.up);
    this.#forced_cursor.off();
    const close_thr = 50;
    if (this.#editor_area.width < close_thr) {
      this.#editor_area.width = 0;
    }
    MyEvent.Ret.call("resize_editor", this.#editor_area.width);
  }
  #callDraw() {
    const srcs = this.export();
    const res = MyEvent.Ret.call("draw_sources", srcs);
    MyEvent.Ret.call("sources_valid", res);
  }
  import(data) { MyEvent.Mul.call("import_source", data); }
  export() { return MyEvent.Ret.call("export_source"); }
};

class EditorArea extends ElementBase {
  #header = new EditorHeader(this);
  #items = new EditorItemsContainer(this);
  constructor(parent) {
    super(parent);
    this.element.appendChild(this.#header.element);
    this.element.appendChild(this.#items.element);
    this.element.id = "editor_area";
    this.#header.id = "header";
  }
};

class EditorHeader extends ElementBase {
  #buttons = new Map ([
    ["push", document.createElement("div")],
    ["load", document.createElement("div")],
    ["save", document.createElement("div")],
    ["new", document.createElement("div")],
    ["img", document.createElement("div")]
  ]);
  constructor(parent) {
    super(parent);
    this.element.id = "header";
    this.#buttons.forEach((item, key) => {
      this.element.appendChild(item);
      item.classList.add("header_btn");
      item.innerText = key;
    });
    this.#registerCallback();
  }
  #registerCallback() {
    this.#buttons.get("push").addEventListener("mousedown", () => this.#pressPush());
    this.#buttons.get("load").addEventListener("mousedown", () => this.#pressLoad());
    this.#buttons.get("save").addEventListener("mousedown", () => this.#pressSave());
    this.#buttons.get("new").addEventListener("mousedown", () => this.#pressNew());
    this.#buttons.get("img").addEventListener("mousedown", () => this.#pressImg());
  }
  #pressNew() {
    if (confirm("remove all filters and start a new project\nare you sure?")) {
      MyEvent.Ret.call("reset_editoritem");
    }
  }
  #pressPush() { MyEvent.Ret.call("push_editoritem") }
  #pressLoad() {
    const input = document.createElement("input");
    input.type = "file";
    input.accept = ".pictfilt";
    input.addEventListener("input", () => {
      if (input.files.length == 0) {
        return;
      }
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        const data = JSON.parse(reader.result);
        MyEvent.Mul.call("import_source", data.sources);
        MyEvent.Mul.call("change_source");
      });
      reader.readAsText(input.files[0]);
    });
    input.click();
  }
  #pressSave() {
    const name = window.prompt("enter a filter name");
    if (name === null) {
      return;
    }
    const data = {sources: MyEvent.Ret.call("export_source")};
    const json = JSON.stringify(data, null, 2);
    const blob = new Blob([json], { type: "application/json" });
    const a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.download = name !== "" ? name + ".pictfilt" : "myfilter.pictfilt";
    a.click();
    URL.revokeObjectURL(a.href);
  }
  #pressImg() {
    MyEvent.Ret.call("dest_png").then(blob => {
      const url = URL.createObjectURL(blob);
      window.open(url, "pictfilt");
      URL.revokeObjectURL(url);
    });
  }
};

class EditorItemsContainer extends ElementBase {
  #list = new List();
  #editor = new Editor(this);
  #sortmode = false;
  #sortitem = null;
  #grabbing = new ForcedCursor("grabbing");
  #callback = () => MyEvent.Ret.call("leave_sortmode", null);
  constructor(parent) {
    super(parent);
    this.element.id = "items_container";
    this.#registerCallback();
  }
  get editor() { return this.#editor; }
  get sortMode() { return this.#sortmode; }
  #registerCallback() {
    MyEvent.Mul.add("import_source", data => this.#import(data));
    MyEvent.Ret.add("export_source", () => this.#export());
    MyEvent.Ret.add("push_editoritem", () => this.#push());
    MyEvent.Ret.add("pop_editoritem", item => this.#pop(item));
    MyEvent.Ret.add("reset_editoritem", () => this.#reset());
    MyEvent.Ret.add("get_editor", () => this.#editor);
    MyEvent.Ret.add("enter_sortmode", item => this.#enterSortMode(item));
    MyEvent.Ret.add("leave_sortmode", (item, pos) => this.#leaveSortMode(item, pos));
    MyEvent.Ret.add("sources_valid", arr => this.#setValid(arr));
  }
  #clear() {
    this.#editor.removeParent();
    while (this.element.firstChild) {
      this.element.removeChild(this.element.firstChild);
    }
    this.#list.clear();
  }
  #import(data) {
    this.#clear();
    data.forEach(item => this.#push().data = item);
    MyEvent.Mul.call("change_source");
  }
  #export() {
    this.#editor.saveSource();
    const arr = new Array();
    this.#list.forEach(item => arr.push(item.data));
    return arr;
  }
  #push() {
    const tmp = new EditorItem(this);
    this.element.appendChild(tmp.element);
    this.#list.push(tmp);
    return tmp;
  }
  #pop(item) {
    this.element.removeChild(item.element);
    this.#list.pop(item);
    MyEvent.Mul.call("change_source");
  }
  #reset() {
    this.#clear();
    this.#push();
    MyEvent.Mul.call("change_source");
  }
  #enterSortMode(item) {
    this.#sortmode = true;
    this.#sortitem = item;
    this.#sortitem.element.classList.add("item_select_sort");
    this.#grabbing.on();
    window.addEventListener("mouseup", this.#callback);
  }
  #leaveSortMode(item, pos) {
    if (item !== null && item !== this.#sortitem) {
      let change_flag = false;
      this.element.removeChild(this.#sortitem.element);
      switch (pos) {
        case "before":
          this.element.insertBefore(this.#sortitem.element, item.element);
          this.#list.insertBefore(this.#list.pop(this.#sortitem), item);
          change_flag = true;
          break;
        case "after":
          const next = item.element.nextSibling;
          if (next === null) {
            this.element.appendChild(this.#sortitem.element);
          } else {
            this.element.insertBefore(this.#sortitem.element, next);
          }
          this.#list.insertAfter(this.#list.pop(this.#sortitem), item);
          change_flag = true;
          break;
        default:
          break;
      }
      if (change_flag) {
        MyEvent.Mul.call("change_source");
      }
    }
    this.#sortitem.element.classList.remove("item_select_sort");
    this.#sortmode = false;
    this.#sortitem = null;
    this.#grabbing.off();
    window.removeEventListener("mouseup", this.#callback);
  }
  #setValid(arr) {
    this.#list.forEach((item, index) => {
      item.valid = arr[index] === null || arr[index];
    });
  }
}

class EditorItem extends ElementBase {
  #data = {
    name: "new filter",
    src: "void main(void) {\n\t_output = texture(_input, _vt);\n}",
    mute: false,
    resize: {
      enable: false,
      value: [null, null]
    },
    scale: {
      enable: false,
      value: [null, null]
    }
  };
  #header = new ItemHeader(this);
  #editor = null;
  #valid = true;
  #callback = {
    move: e => this.#move(e),
    up: e => this.#up(e),
    out: () => this.#out(),
    setOpen: flag => this.setOpen(flag),
    setMute: flag => this.setMute(flag)
  }
  constructor(parent) {
    super(parent);
    this.element.appendChild(this.#header.element);
    this.element.classList.add("item");
    this.element.id = "";
    this.#header.name = "new filter";
    this.#registerCallback();
  }
  get data() { return this.#data; }
  get name() { return this.#data.name; }
  get src() { return this.#data.src; }
  get mute() { return this.#data.mute; }
  get resize() { return this.#data.resize; }
  get scale() { return this.#data.scale; }
  get valid() { return this.#valid; }
  set data(obj) {
    this.name = obj.name;
    this.src = obj.src;
    this.mute = obj.mute;
    this.resize = obj.resize;
    this.scale = obj.scale;
  }
  set name(str) {
    this.#data.name = str;
    this.#header.name = this.#data.name;
  }
  set src(str) { this.#data.src = str; }
  set mute(bool) {
    this.#data.mute = bool;
    this.#header.soloFlag = false;
    this.#header.muteFlag = this.#data.mute;
  }
  set resize(obj) { XYInput.setDataHelper(this.#data.resize, obj); }
  set scale(obj) { XYInput.setDataHelper(this.#data.scale, obj); }
  set valid(bool) {
    this.#valid = bool;
    this.#header.valid = this.#valid;
  }
  #registerCallback() {
    this.element.addEventListener("mouseover", () =>this.#over());
    MyEvent.Mul.add("set_itemopen", this.#callback.setOpen);
    MyEvent.Mul.add("set_itemmute", this.#callback.setMute);
  }
  #removeCallback() {
    MyEvent.Mul.remove("set_itemopen", this.#callback.setOpen);
    MyEvent.Mul.remove("set_itemmute", this.#callback.setMute);
  }
  #over() {
    if (this.parent.sortMode) {
      this.element.addEventListener("mousemove", this.#callback.move);
      this.element.addEventListener("mouseup", this.#callback.up);
      this.element.addEventListener("mouseout", this.#callback.out);
    }
  }
  #move(e) {
    if (this.element.clientHeight / 2 > e.offsetY) {
      this.element.classList.add("item_over_top");
      this.element.classList.remove("item_over_bottom");
    } else {
      this.element.classList.remove("item_over_top");
      this.element.classList.add("item_over_bottom");
    }
  }
  #up(e) {
    const pos = this.element.clientHeight / 2 > e.offsetY ? "before" : "after";
    MyEvent.Ret.call("leave_sortmode", this, pos);
    this.#out();
  }
  #out() {
    this.element.classList.remove("item_over_top");
    this.element.classList.remove("item_over_bottom");
    this.element.removeEventListener("mousemove", this.#callback.move);
    this.element.removeEventListener("mouseup", this.#callback.up);
    this.element.removeEventListener("mouseout", this.#callback.out);
  }
  #setEditor(editor) {
    if (this.#editor === null) {
      this.element.appendChild(editor.element);
      this.#editor = editor;
      this.#editor.setParent(this);
    }
  }
  #popEditor() {
    if (this.#editor !== null) {
      this.element.removeChild(this.#editor.element);
      this.#editor.removeParent();
      this.#editor = null;
    }
  }
  setOpen(flag) {
    if (flag) {
      this.#header.openFlag = true;
      this.#setEditor(MyEvent.Ret.call("get_editor"));
    } else {
      this.#header.openFlag = false;
      this.#popEditor();
    }
  }
  setSolo(flag) {
    if (flag) {
      MyEvent.Mul.call("set_itemmute", true);
      this.mute = false;
      this.#header.soloFlag = true;
    } else {
      MyEvent.Mul.call("set_itemmute", false);
    }
  }
  setMute(flag) { this.mute = flag; }
  suicide() {
    if (confirm("this filter will be removed\nare you sure?")) {
      this.#removeCallback();
      MyEvent.Ret.call("pop_editoritem", this);
    }
  }
}

class ItemHeader extends ElementBase {
  #open_btn = document.createElement("div");
  #solo_btn = document.createElement("div");
  #mute_btn = document.createElement("div");
  #name = document.createElement("div");
  #open_flag = false;
  #solo_flag = false;
  constructor(parent) {
    super(parent);
    this.element.appendChild(this.#open_btn);
    this.element.appendChild(this.#solo_btn);
    this.element.appendChild(this.#mute_btn);
    this.element.appendChild(this.#name);
    this.element.classList.add("item_header");
    this.#open_btn.classList.add("item_open");
    this.#open_btn.innerText = "▼";
    this.#solo_btn.classList.add("item_solo");
    this.#solo_btn.innerText = "S";
    this.#mute_btn.classList.add("item_mute");
    this.#mute_btn.innerText = "M";
    this.#name.classList.add("item_name");
    this.#registerCallback();
  }
  set name(str) { this.#name.innerText = str; }
  set openFlag(bool) {
    this.#open_btn.innerText = bool ? "▲" : "▼";
    this.#open_flag = bool;
  }
  set soloFlag(bool) {
    bool ? this.#solo_btn.classList.add("item_emphasis")
      : this.#solo_btn.classList.remove("item_emphasis");
    this.#solo_flag = bool;
  }
  set muteFlag(bool) {
    bool ? this.#mute_btn.classList.add("item_emphasis")
      : this.#mute_btn.classList.remove("item_emphasis");
  }
  set valid(bool) {
    bool ? this.#name.classList.remove("item_emphasis")
      : this.#name.classList.add("item_emphasis");
  }
  #registerCallback() {
    this.#open_btn.addEventListener("mousedown", () => this.#clickOpen());
    this.#solo_btn.addEventListener("mousedown", () => this.#clickSolo());
    this.#mute_btn.addEventListener("mousedown", () => this.#clickMute());
    this.#name.addEventListener("mousedown", () => MyEvent.Ret.call("enter_sortmode", this.parent));
  }
  #clickOpen() {
    const flag = !this.#open_flag;
    MyEvent.Mul.call("set_itemopen", false);
    this.parent.setOpen(flag);
  }
  #clickSolo() {
    this.parent.setSolo(!this.#solo_flag);
    MyEvent.Mul.call("change_source");
  }
  #clickMute() {
    this.parent.setMute(!this.parent.mute);
    MyEvent.Mul.call("change_source");
  }
}

class Editor extends ElementBase {
  #header = document.createElement("div");
  #name_ipt = new NameInput(this, "name");
  #res_ipt = new XYInput(this, "resize[x,y]", 1, 4096, 1);
  #scale_ipt = new XYInput(this, "scale[x,y]", 0.001, 10.0, 0.001);
  #remove_btn = document.createElement("div");
  #text = document.createElement("div");
  #message = document.createElement("div");
  #editor;
  #callback = () => this.#onChange(); 
  constructor() {
    super(null);
    this.element.classList.add("item_editor");
    this.element.appendChild(this.#header);
    this.element.appendChild(this.#text);
    this.element.appendChild(this.#message);
    this.#header.appendChild(this.#name_ipt.element);
    this.#header.appendChild(this.#res_ipt.element);
    this.#header.appendChild(this.#scale_ipt.element);
    this.#header.appendChild(this.#remove_btn);
    this.#header.classList.add("editor_header")
    this.#remove_btn.innerText = "remove";
    this.#remove_btn.classList.add("remove_btn");
    this.#text.classList.add("text");
    this.#message.classList.add("message");
    this.#InitAce();
    this.#registerCallback();
  }
  #InitAce() {
    this.#editor = ace.edit(this.#text, {
      mode: "ace/mode/glsl",
      theme: "ace/theme/twilight",
      enableBasicAutocompletion: true,
      enableLiveAutocompletion: true,
      enableWheelValue: true,
      showPrintMargin: false,
      useSoftTabs: true,
      tabSize: 2
    });
  }
  #registerCallback() {
    this.#name_ipt.onChange(() => this.parent.name = this.#name_ipt.name);
    this.#res_ipt.onChange(() => {
      this.parent.resize = this.#res_ipt.data;
      MyEvent.Mul.call("change_source");
    });
    this.#scale_ipt.onChange(() => {
      this.parent.scale = this.#scale_ipt.data;
      MyEvent.Mul.call("change_source");
    });
    this.#remove_btn.addEventListener("click", () => this.parent.suicide());
    MyEvent.Ret.add("resize_editor", () => this.#editor.resize(true));
  }
  #onChange() {
    const res = MyEvent.Ret.call("check_source", this.#editor.getValue());
    this.parent.valid = res.valid;
    if  (res.valid) {
      this.#message.innerText = "compile succeed";
      MyEvent.Mul.call("change_source");
    } else {
      this.#message.innerText = res.log;
    }
  }
  setParent(parent) {
    this.parent = parent;
    this.#editor.setValue(this.parent.src);
    this.#editor.moveCursorTo(0, 0);
    this.#editor.getSession().on("tokenizerUpdate", this.#callback);
    this.#name_ipt.name = this.parent.name;
    this.#res_ipt.data = this.parent.resize;
    this.#scale_ipt.data = this.parent.scale;
  }
  removeParent() {
    this.saveSource();
    this.parent = null;
    this.#message.innerText = "";
    this.#editor.getSession().off("tokenizerUpdate", this.#callback);
  }
  saveSource() {
    if (this.parent !== null) {
      this.parent.src = this.#editor.getValue();
    }
  }
};

class NameInput extends ElementBase {
  #desc = document.createElement("div");
  #input = document.createElement("input");
  constructor(parent, description) {
    super(parent);
    this.element.appendChild(this.#desc);
    this.element.appendChild(this.#input);
    this.#desc.innerText = description;
    this.element.classList.add("nameinput_container");
    this.#desc.classList.add("nameinput_description");
    this.#input.classList.add("nameinput_input");
  }
  get name() { return this.#input.value; }
  set name(str) { this.#input.value = str; }
  onChange(callback) {
    this.#input.addEventListener("change", callback);
  }
};

class XYInput extends ElementBase {
  #desc = document.createElement("div");
  #enable = false;
  #input = [document.createElement("input"), document.createElement("input")];
  constructor(parent, description, min, max, step) {
    super(parent);
    this.element.appendChild(this.#desc);
    this.#input.forEach(item => this.element.appendChild(item));
    this.#desc.innerText = description;
    this.element.classList.add("xyinput_container");
    this.#desc.classList.add("xyinput_description", "xyinput_description_disable");
    this.#desc.addEventListener("mousedown", () => this.enable = !this.enable);
    this.#input.forEach(item => {
      item.classList.add("xyinput_input");
      item.type = "number";
      item.min = min;
      item.max = max;
      item.step = step;
    });
    this.#updateInputEnable();
  }
  get enable() { return this.#enable; }
  get value() { return [this.#input[0].value, this.#input[1].value]; }
  get data() { return {enable: this.enable, value: this.value} };
  set enable(bool) {
    this.#enable = bool;
    this.#updateInputEnable();
  }
  set value(arr) { this.#input.forEach((item, index) => item.value = arr[index]); }
  set data(obj) {
    this.enable = obj.enable;
    this.value = obj.value;
  }
  #updateInputEnable() {
    this.#input.forEach(item => item.disabled = !this.#enable);
    if (this.#enable) {
      this.#desc.classList.remove("xyinput_description_disable");
    } else {
      this.#desc.classList.add("xyinput_description_disable");
    }
  }
  onChange(callback) {
    this.#desc.addEventListener("mousedown", callback);
    this.#input.forEach(item => item.addEventListener("change", callback));
  }
  static setDataHelper(lhs, rhs) {
    switch (typeof(rhs)) {
      case "boolean":
        lhs.enable = rhs;
        break;
      case "object":
        if (Array.isArray(rhs)) {
          lhs.value[0] = rhs[0];
          lhs.value[1] = rhs[1];
        } else {
          lhs.enable = rhs.enable;
          lhs.value = rhs.value;
        }
      default:
        break;
    }
  }
};