import * as MyEvent from "./event.mjs"
import DitherBinary from "./dither.mjs"

export default class CanvasMgr {
  #container;
  #canvas = document.createElement("canvas");
  #renderer;
  #resize_obs = new ResizeObserver(() => this.#setCanvasResolution());
  #callback = {
    move: e => this.#mouseMove(e),
    up: () => this.#mouseUp()
  };
  constructor(container, img = null) {
    this.#container = container;
    this.#container.appendChild(this.#canvas);
    this.#canvas.id = "canvas";
    this.#renderer = new GLRenderer(this.#canvas.getContext("webgl2", { antialias: false }), img);
    this.#registerCallback();
  }
  #registerCallback() {
    const query = `(resolution: ${window.devicePixelRatio}dppx)`;
    matchMedia(query).addEventListener("change", () => this.#setCanvasResolution());
    this.#resize_obs.observe(this.#canvas);
    MyEvent.Ret.add("canvas_res", () => [this.#canvas.width, this.#canvas.height]);
    this.#canvas.addEventListener("contextmenu", (e) => e.preventDefault());
    this.#canvas.addEventListener("mousedown", e => this.#mouseDown(e));
    this.#canvas.addEventListener("wheel", e => MyEvent.Mul.call("canvas_wheel", e.deltaY));
  }
  #mouseDown(e) {
    switch (e.button) {
      case 0:
        this.#canvas.addEventListener("mousemove", this.#callback.move);
        this.#canvas.addEventListener("mouseup", this.#callback.up);
        break;
      case 1:
        MyEvent.Mul.call("reset_scales");
        break;
      case 2:
        MyEvent.Mul.call("fit_scale");
        break;
      default:
        break;
    }
  }
  #mouseMove(e) {
    MyEvent.Mul.call("canvas_move", { x: e.movementX, y: e.movementY });
  }
  #mouseUp() {
    this.#canvas.removeEventListener("mousemove", this.#callback.move);
    this.#canvas.removeEventListener("mouseup", this.#callback.up);
  }
  #setCanvasResolution() {
    const ratio = window.devicePixelRatio;
    const width = this.#canvas.getBoundingClientRect().width * ratio;
    const height = this.#canvas.getBoundingClientRect().height * ratio;
    this.#canvas.width = width;
    this.#canvas.height = height;
    MyEvent.Mul.call("draw");
  }
  setImg(img) { this.#renderer.img = img; }
};

class GLRenderer {
  #gl;
  #img_tex;
  #frame;
  #dest_tex;
  constructor(gl, img) {
    this.#gl = gl;
    this.#img_tex = new ImgTexture(this.#gl, img);
    this.#frame = new FrameDraw(this.#gl);
    this.#dest_tex = new DestTexture(this.#gl, this.#img_tex.resolution);
    this.#initGL();
    this.#initDraw();
    RenderShader.init(this.#gl, img);
    this.#registerCallback();
  }
  #initGL() {
    this.#gl.disable(this.#gl.DEPTH_TEST);
    // this.#gl.enable(this.#gl.SCISSOR_TEST);
  }
  #initDraw() {
    const shader = new PrintShader(this.#gl);
    shader.use(this.#img_tex.texture, this.#img_tex.resolution);
    this.#frame.drawTexture(this.#dest_tex.texture, this.#dest_tex.resolution);
    shader.delete();
  }
  #registerCallback() {
    MyEvent.Ret.add("check_source", src => this.#checkSource(src));
    MyEvent.Ret.add("draw_sources", srcs => this.#drawSources(srcs));
    MyEvent.Mul.add("draw", () => this.#draw());
    MyEvent.Ret.add("dest_res", () => this.#dest_tex.resolution);
    MyEvent.Ret.add("dest_png", () => this.#dest2PNG());
  }
  #checkSource(src) {
    const shader = new RenderShader(this.#gl, src);
    const res = { valid: shader.valid, log: shader.log };
    shader.delete();
    return res;
  }
  static #resizeHelper(resolution, resize, scale) {
    let fix_res = resolution;
    if (resize.enable) {
      const mul = Math.min(resize.value[0] / fix_res[0], resize.value[1] / fix_res[1]);
      fix_res = fix_res.map(n => n * mul);
    }
    if (scale.enable) {
      fix_res = fix_res.map((n, i) => n * scale.value[i]);
    }
    fix_res = fix_res.map(n => Math.max(Math.floor(n), 1));
    return fix_res;
  }
  #drawSources(srcs) {
    const valid = new Array(srcs.length);
    const buf = new FlipTextureBuffer(this.#img_tex);
    srcs.forEach((src, index) => {
      if (src.mute) {
        valid[index] = null;
        return;
      }
      const shader = new RenderShader(this.#gl, src.src);
      if (shader.valid) {
        valid[index] = true;
        const fix_res = GLRenderer.#resizeHelper(buf.input.resolution, src.resize, src.scale);
        buf.output = new NullTexture(this.#gl, fix_res);
        shader.use(buf.input.texture, buf.output.resolution);
        this.#frame.drawTexture(buf.output.texture, buf.output.resolution);
        buf.flip();
      } else {
        valid[index] = false;
        return;
      }
      shader.delete();
    });
    const shader = new PrintShader(this.#gl);
    this.#dest_tex.resolution = buf.input.resolution;
    shader.use(buf.input.texture, this.#dest_tex.resolution);
    this.#frame.drawTexture(this.#dest_tex.texture, this.#dest_tex.resolution);
    shader.delete();
    buf.delete();
    this.#draw();
    return valid;
  }
  #draw() {
    this.#frame.drawCanvas(this.#dest_tex.texture, this.#dest_tex.resolution);
  }
  #dest2PNG() {
    const buf = this.#frame.readPixels(this.#dest_tex.texture, this.#dest_tex.resolution);
    const img_data = new ImageData(buf, this.#dest_tex.resolution[0]);
    const cv = document.createElement("canvas");
    cv.width = img_data.width;
    cv.height = img_data.height;
    const ctx = cv.getContext("2d");
    ctx.putImageData(img_data, 0, 0);
    return new Promise(resolve => cv.toBlob(resolve));
  }
};

class ShaderBase {
  static #vert_src = "#version 300 es\nlayout(location=0)in vec2 vp;layout(location=1)in vec2 vt;out vec2 _vt;void main(void){gl_Position=vec4(vp,.0,1.);_vt=vt;}";
  static #frag_header = "#version 300 es\nprecision mediump float;layout(location=0)out vec4 _output;uniform sampler2D _input;uniform vec2 _res;in vec2 _vt;";
  static #frag_line_adjust_base = 2;
  #frag_line_adjust = ShaderBase.#frag_line_adjust_base;
  #gl;
  #valid = false;
  #log = "";
  #program = null;
  #uniform = { input: null, resolution: null }
  constructor(gl, uniform_src, defs_arr, src) {
    this.#gl = gl;
    this.#frag_line_adjust += defs_arr.length;
    const vert = this.#createVertexShader(ShaderBase.#vert_src);
    if (!this.valid) return;
    const frag_src = ShaderBase.#frag_header + uniform_src + "\n" + defs_arr.join("\n") + "\n" + src;
    const frag = this.#createFragmentShader(frag_src);
    if (!this.valid) {
      this.#logLineFix();
      return;
    }
    this.#linkProgram(vert, frag);
  }
  get valid() { return this.#valid; }
  get log() { return this.#log; }
  get program() { return this.#program; }
  #createShader(source, type) {
    const shader = this.#gl.createShader(type);
    this.#gl.shaderSource(shader, source);
    this.#gl.compileShader(shader);
    if (this.#gl.getShaderParameter(shader, this.#gl.COMPILE_STATUS)) {
      this.#valid = true;
      return shader;
    } else {
      this.#valid = false;
      this.#log = this.#gl.getShaderInfoLog(shader);
      return null;
    }
  }
  #createVertexShader(source) {
    return this.#createShader(source, this.#gl.VERTEX_SHADER);
  }
  #createFragmentShader(source) {
    return this.#createShader(source, this.#gl.FRAGMENT_SHADER);
  }
  #linkProgram(vertex, fragment) {
    const program = this.#gl.createProgram();
    this.#gl.attachShader(program, vertex);
    this.#gl.attachShader(program, fragment);
    this.#gl.linkProgram(program);
    if (this.#gl.getProgramParameter(program, this.#gl.LINK_STATUS)) {
      this.#valid = true;
      this.#program = program;
      this.#uniform.input = this.#gl.getUniformLocation(this.#program, "_input");
      this.#uniform.resolution = this.#gl.getUniformLocation(this.#program, "_res");
    } else {
      this.#valid = false;
      this.#log = this.#gl.getProgramInfoLog(program);
    }
  }
  #logLineFix() {
    const str_arr = this.#log.split('\n');
    if (str_arr[str_arr.length - 1] === '\u0000') {
      str_arr.pop();
    }
    this.#log = "";
    str_arr.forEach(line => {
      const spl = line.split(':');
      spl[2] = Number(spl[2]) - this.#frag_line_adjust;
      this.#log += spl.join(':') + '\n';
    });
  }
  use(texture, resolution) {
    this.#gl.useProgram(this.#program);
    this.#gl.activeTexture(this.#gl.TEXTURE0);
    this.#gl.bindTexture(this.#gl.TEXTURE_2D, texture);
    this.#gl.uniform1i(this.#uniform.input, 0);
    this.#gl.uniform2fv(this.#uniform.resolution, resolution);
  }
  delete() {
    this.#gl.deleteProgram(this.#program);
    this.#program = null;
    this.#valid = false;
    this.#log = "deleted";
  }
};

class DitherShader extends ShaderBase {
  static #size = 128;
  static #uniform_src = "uniform sampler2D _t_;";
  static #defs_arr = [
    `#define _dbase_(s) texture(_t_,gl_FragCoord.xy/vec2(${DitherShader.#size}.*float(s)))`,
    "#define _whiteNoise(s) _dbase_(s).r",
    "#define _blueNoise(s) _dbase_(s).g",
    "#define _bayer(s) _dbase_(s).b",
    "#define _checker(s) _dbase_(s).a"
  ]
  static #parent_gl = null;
  static #texture = null;
  static #data = null;
  #uniform = null;
  constructor(gl, uniform_src, defs_arr, src) {
    super(gl, DitherShader.#uniform_src + uniform_src, DitherShader.#defs_arr.concat(defs_arr), src);
    if (DitherShader.#parent_gl === null ||
      DitherShader.#parent_gl !== gl ||
      DitherShader.#texture === null) {
      DitherShader.#init(gl);
    }
    if (this.valid) {
      this.#uniform = DitherShader.#parent_gl.getUniformLocation(this.program, "_t_");
    }
  }
  static #init(gl) {
    DitherShader.#parent_gl = gl;
    if (DitherShader.#texture !== null) {
      DitherShader.#parent_gl.deleteTexture(DitherShader.#texture);
    }
    if (DitherShader.#data === null) {
      DitherShader.#data = DitherBinary;
    }
    DitherShader.#texture = DitherShader.#parent_gl.createTexture();
    DitherShader.#parent_gl.bindTexture(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#texture);
    DitherShader.#parent_gl.texImage2D(DitherShader.#parent_gl.TEXTURE_2D, 0, DitherShader.#parent_gl.RGBA, DitherShader.#size, DitherShader.#size, 0, DitherShader.#parent_gl.RGBA, DitherShader.#parent_gl.UNSIGNED_BYTE, DitherShader.#data);
    DitherShader.#parent_gl.texParameteri(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#parent_gl.TEXTURE_MAG_FILTER, DitherShader.#parent_gl.NEAREST);
    DitherShader.#parent_gl.texParameteri(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#parent_gl.TEXTURE_MIN_FILTER, DitherShader.#parent_gl.NEAREST);
    DitherShader.#parent_gl.texParameteri(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#parent_gl.TEXTURE_WRAP_S, DitherShader.#parent_gl.REPEAT);
    DitherShader.#parent_gl.texParameteri(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#parent_gl.TEXTURE_WRAP_T, DitherShader.#parent_gl.REPEAT);
    DitherShader.#parent_gl.bindTexture(DitherShader.#parent_gl.TEXTURE_2D, null);
  }
  use(texture, resolution) {
    super.use(texture, resolution);
    DitherShader.#parent_gl.activeTexture(DitherShader.#parent_gl.TEXTURE1);
    DitherShader.#parent_gl.bindTexture(DitherShader.#parent_gl.TEXTURE_2D, DitherShader.#texture);
    DitherShader.#parent_gl.uniform1i(this.#uniform, 1);
  }
};

function img2bin(img) {
  const cv = document.createElement("canvas");
  cv.width = img.width;
  cv.height = img.height;
  const ctx = cv.getContext("2d");
  ctx.drawImage(img, 0, 0);
  return ctx.getImageData(0, 0, cv.width, cv.height);
}

function rgb2rgbl(r, g, b) {
  return [r, g, b, Math.floor(r * 0.21 + g * 0.72 + b * 0.07)];
}

function rgbl2hsvx(rgbl) {
  const max = Math.max(rgbl[0], rgbl[1], rgbl[2]);
  const min = Math.min(rgbl[0], rgbl[1], rgbl[2]);
  const sub = max - min;
  const buffer = new Array(4);
  switch (max) {
    case 0:
      return buffer.fill(0);
    case min:
      buffer[0] = 0;
      break;
    case rgbl[0]:
      buffer[0] = 60 * ((rgbl[1] - rgbl[2]) / sub);
      break;
    case rgbl[1]:
      buffer[0] = 60 * ((rgbl[2] - rgbl[0]) / sub) + 120;
      break;
    case rgbl[2]:
      buffer[0] = 60 * ((rgbl[0] - rgbl[1]) / sub) + 240;
      break;
  }
  buffer[0] = buffer[0] < 0 ? Math.round((buffer[0] + 360) * (255 / 360))
    : Math.round(buffer[0] * (255 / 360));
  buffer[1] = Math.round((sub / max) * 255);
  buffer[2] = max;
  buffer[3] = min;
  return buffer;
}

function histogram2histogramImg(histogram) {
  const h_img = new Uint8ClampedArray(256 * histogram.length * 2);
  const bin_width = 256 * 4;
  const writer = (line, color_offset, type_offset) => {
    const hst_index = color_offset + (type_offset * 4);
    for (let i = 0, pixel = 0; i < bin_width; i += 4, ++pixel) {
      const img_index = (line * bin_width) + i + color_offset;
      h_img[img_index] = histogram[hst_index][pixel].v;
    }
    ++line;
    histogram[hst_index].sort((a, b) => b.v - a.v);
    for (let i = 0, pixel = 0; i < bin_width; i += 4, ++pixel) {
      const img_index = (line * bin_width) + i + color_offset;
      h_img[img_index] = histogram[hst_index][pixel].i;
    }
  };
  for (let c = 0; c < 4; ++c) {
    writer(0, c, 0);
    writer(2, c, 1);
  }
  return h_img;
}

function normalizeHistgram(histogram) {
  for (let i = 0, len = histogram.length; i < len; i += 4) {
    let max = 0;
    for (let j = 0; j < 4; ++j) {
      max = histogram[i + j].reduce((a, n) => Math.max(a, n.v), max);
    }
    max /= 255;
    for (let j = 0; j < 4; ++j) {
      for (let k = 0, len = histogram[i + j].length; k < len; ++k) {
        histogram[i + j][k].v = Math.round(histogram[i + j][k].v / max);
      }
    }
  }
  return histogram;
}

function img2histogramImg(img, iterate = 0) {
  const pixels = img !== null ? img2bin(img).data : new Uint8ClampedArray(4);
  const pix_len = pixels.length / 4;
  if (iterate === 0 || iterate > pix_len) {
    iterate = pix_len;
  }
  const inc = iterate < pix_len ? (i) => Math.floor(Math.random() * pix_len) * 4
    : (i) => i + 4;
  const make_buf = () => Array.from({ length: 256 }, (v, i) => { return { v: 0, i: i }; });
  const histogram = Array.from({ length: 8 }, make_buf);
  const average = (new Array(8)).fill(0);
  const write = (offset = 0) => {
    return (n, i) => {
      average[i + offset] += n;
      ++histogram[i + offset][n].v;
    }
  };
  for (let i = inc(-4), count = 0; count < iterate; ++count, i = inc(i)) {
    const rgbl = rgb2rgbl(pixels[i + 0], pixels[i + 1], pixels[i + 2]);
    const hsvx = rgbl2hsvx(rgbl);
    rgbl.forEach(write(0));
    hsvx.forEach(write(4));
  }
  normalizeHistgram(histogram);
  for (let i = 0; i < average.length; ++i) {
    average[i] = Math.round(average[i] / iterate) / 255;
  }
  return {
    img: histogram2histogramImg(histogram),
    width: 256,
    height: 4,
    rgb: average.slice(0, 4),
    hsv: average.slice(4, 8)
  };
}

class RenderShader extends DitherShader {
  static #uniform_src = "uniform sampler2D _h_;uniform vec4 _rgbAverage;uniform vec4 _hsvAverage;";
  static #defs_arr = [
    "#define _hbase_(x,y) texture(_h_,vec2(x,y))",
    "#define _rgbHistogram(x) _hbase_(x,0.125)",
    "#define _rgbHistogramRank(x) _hbase_(x,0.375)",
    "#define _rgbPeak _rgbHistogramRank(0.0)",
    "#define _hsvHistogram(x) _hbase_(x,0.625)",
    "#define _hsvHistogramRank(x) _hbase_(x,0.875)",
    "#define _hsvPeak _hsvHistogramRank(0.0)"
  ]
  static #parent_gl = null;
  static #texture = null;
  static #data = null;
  #uniform = {
    texture: null,
    rgb: null,
    hsv: null
  };
  constructor(gl, src) {
    super(gl, RenderShader.#uniform_src, RenderShader.#defs_arr, src);
    if (RenderShader.#parent_gl === null ||
      RenderShader.#parent_gl !== gl ||
      RenderShader.#texture === null) {
      RenderShader.init(gl, null);
    }
    if (this.valid) {
      this.#uniform.texture = RenderShader.#parent_gl.getUniformLocation(this.program, "_h_");
      this.#uniform.rgb = RenderShader.#parent_gl.getUniformLocation(this.program, "_rgbAverage");
      this.#uniform.hsv = RenderShader.#parent_gl.getUniformLocation(this.program, "_hsvAverage");
    }
  }
  static init(gl, img) {
    RenderShader.#parent_gl = gl;
    if (RenderShader.#texture !== null) {
      RenderShader.#parent_gl.deleteTexture(RenderShader.#texture);
    }
    if (RenderShader.#data === null) {
      RenderShader.#data = img2histogramImg(img, 65536);
    }
    RenderShader.#texture = RenderShader.#parent_gl.createTexture();
    RenderShader.#parent_gl.bindTexture(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#texture);
    RenderShader.#parent_gl.texImage2D(RenderShader.#parent_gl.TEXTURE_2D, 0, RenderShader.#parent_gl.RGBA, RenderShader.#data.width, RenderShader.#data.height, 0, RenderShader.#parent_gl.RGBA, RenderShader.#parent_gl.UNSIGNED_BYTE, RenderShader.#data.img);
    RenderShader.#parent_gl.texParameteri(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#parent_gl.TEXTURE_MAG_FILTER, RenderShader.#parent_gl.NEAREST);
    RenderShader.#parent_gl.texParameteri(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#parent_gl.TEXTURE_MIN_FILTER, RenderShader.#parent_gl.NEAREST);
    RenderShader.#parent_gl.texParameteri(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#parent_gl.TEXTURE_WRAP_S, RenderShader.#parent_gl.CLAMP_TO_EDGE);
    RenderShader.#parent_gl.texParameteri(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#parent_gl.TEXTURE_WRAP_T, RenderShader.#parent_gl.CLAMP_TO_EDGE);
    RenderShader.#parent_gl.bindTexture(RenderShader.#parent_gl.TEXTURE_2D, null);
  }
  use(texture, resolution) {
    super.use(texture, resolution);
    RenderShader.#parent_gl.activeTexture(RenderShader.#parent_gl.TEXTURE2);
    RenderShader.#parent_gl.bindTexture(RenderShader.#parent_gl.TEXTURE_2D, RenderShader.#texture);
    RenderShader.#parent_gl.uniform1i(this.#uniform.texture, 2);
    RenderShader.#parent_gl.uniform4fv(this.#uniform.rgb, RenderShader.#data.rgb);
    RenderShader.#parent_gl.uniform4fv(this.#uniform.hsv, RenderShader.#data.hsv);
  }
};

class PrintShader extends ShaderBase {
  static #frag_src = "void main(void){_output=texture(_input,vec2(_vt.x,1.-_vt.y));}";
  constructor(gl) { super(gl, "", [], PrintShader.#frag_src); }
};

class TextureBase {
  #gl;
  #texture;
  #resolution;
  constructor(gl) {
    this.#gl = gl;
    this.#texture = this.#gl.createTexture();
  }
  get gl() { return this.#gl; }
  get texture() { return this.#texture; }
  get resolution() { return this.#resolution; }
  set resolution(arr) { this.#resolution = arr; }
}

class NullTexture extends TextureBase {
  constructor(gl, resolution) {
    super(gl);
    this.resolution = resolution;
    this.#createTexture();
  }
  #createTexture() {
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.resolution[0], this.resolution[1], 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    this.gl.bindTexture(this.gl.TEXTURE_2D, null);
  }
  delete() { this.gl.deleteTexture(this.texture); }
};

class ImgTexture extends TextureBase {
  #img;
  constructor(gl, img) {
    super(gl);
    this.#img = img;
    this.#createTexture();
  }
  set img(element) {
    this.#img = element;
    this.#createTexture();
  }
  #createTexture() {
    this.resolution = this.#img === null ? [1, 1] : [this.#img.naturalWidth, this.#img.naturalHeight];
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
    this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.resolution[0], this.resolution[1], 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.#img);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, false);
    this.gl.bindTexture(this.gl.TEXTURE_2D, null);
  }
  delete() { }
};

class DestTexture extends TextureBase {
  constructor(gl, resolution) {
    super(gl);
    this.resolution = resolution;
  }
  get resolution() { return super.resolution; }
  set resolution(arr) {
    super.resolution = arr;
    this.#createTexture();
  }
  #createTexture() {
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
    this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.resolution[0], this.resolution[1], 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    this.gl.bindTexture(this.gl.TEXTURE_2D, null);
  }
  delete() { }
};

class FlipTextureBuffer {
  #buffer = [null, null];
  #input = 0;
  #output = 1;
  constructor(init_input) {
    this.#buffer[this.#input] = init_input;
  }
  get input() { return this.#buffer[this.#input]; }
  get output() { return this.#buffer[this.#output]; }
  set output(obj) {
    if (this.#buffer[this.#output] !== null) {
      this.#buffer[this.#output].delete()
    };
    this.#buffer[this.#output] = obj;
  }
  flip() {
    this.#input = (this.#input + 1) % 2;
    this.#output = (this.#output + 1) % 2;
  }
  delete() {
    if (this.#buffer[this.#input] !== null) {
      this.#buffer[this.#input].delete();
    }
    if (this.#buffer[this.#output] !== null) {
      this.#buffer[this.#output].delete();
    }
  }
};

function create2DPanelVAO(gl, layout = 0) {
  const vao = gl.createVertexArray();
  gl.bindVertexArray(vao);
  const vertices = new Float32Array([
    -1.0, -1.0, 0.0, 0.0,
    1.0, -1.0, 1.0, 0.0,
    1.0, 1.0, 1.0, 1.0,
    -1.0, 1.0, 0.0, 1.0
  ]);
  const stride = vertices.BYTES_PER_ELEMENT * 4;
  const vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
  gl.vertexAttribPointer(layout, 2, gl.FLOAT, false, stride, 0);
  gl.enableVertexAttribArray(layout);
  const vt_offset = vertices.BYTES_PER_ELEMENT * 2;
  gl.vertexAttribPointer(layout + 1, 2, gl.FLOAT, false, stride, vt_offset);
  gl.enableVertexAttribArray(layout + 1);
  const indices = new Int8Array([
    0, 1, 2,
    0, 2, 3
  ]);
  const ebo = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
  gl.bindVertexArray(null);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  return {
    vao: vao,
    vbo: vbo,
    ebo: ebo,
    layout: layout,
    layout_len: 2,
    buffer_len: vertices.length,
    index_len: indices.length
  };
}

function clamp(n, min, max) { return Math.min(Math.max(n, min), max); }

class FrameDraw {
  #gl;
  #frame;
  #vao;
  #shader;
  #scale = 1.0;
  #position = [0, 0];
  constructor(gl) {
    this.#gl = gl;
    this.#frame = this.#gl.createFramebuffer();
    this.#vao = create2DPanelVAO(this.#gl);
    this.#shader = new PrintShader(this.#gl);
    this.#registerCallback();
  }
  #registerCallback() {
    MyEvent.Mul.add("reset_scales", () => this.#resetScales());
    MyEvent.Mul.add("fit_scale", () => this.#fitScale());
    MyEvent.Mul.add("canvas_wheel", delta => this.#mouseWheel(delta));
    MyEvent.Mul.add("canvas_move", delta => this.#mouseDrag(delta));
  }
  #resetScales() {
    this.#scale = 1.0;
    this.#position = [0, 0];
    MyEvent.Mul.call("draw");
  }
  #fitScale() {
    const canvas_res = MyEvent.Ret.call("canvas_res");
    const dest_res = MyEvent.Ret.call("dest_res");
    const scale = canvas_res.map((n, i) => n / dest_res[i]);
    this.#scale = Math.min(scale[0], scale[1]);
    this.#position = [0, 0];
    MyEvent.Mul.call("draw");
  }
  #mouseWheel(delta) {
    const thr_d = 300.0;
    const fix_d = clamp(delta, -thr_d, thr_d) / thr_d;
    const mul_scale = fix_d < 0.0 ? Math.abs(1.0 + fix_d / 2.0) : 1.0 + fix_d;
    const scale = clamp(this.#scale * mul_scale, 0.1, 10.0);
    const mul_pos = scale / this.#scale;
    this.#scale = scale;
    this.#position = this.#position.map(n => n * mul_pos);
    MyEvent.Mul.call("draw");
  }
  #mouseDrag(delta) {
    const fix = window.devicePixelRatio;
    this.#position[0] += delta.x * fix;
    this.#position[1] -= delta.y * fix;
    MyEvent.Mul.call("draw");
  }
  drawTexture(output_texture, resolution) {
    this.#gl.viewport(0, 0, resolution[0], resolution[1]);
    // this.#gl.scissor(0, 0, resolution[0], resolution[1]);
    this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frame);
    this.#gl.framebufferTexture2D(this.#gl.FRAMEBUFFER, this.#gl.COLOR_ATTACHMENT0, this.#gl.TEXTURE_2D, output_texture, 0);
    this.#drawImpl(resolution);
  }
  drawCanvas(input_texture, resolution) {
    this.#shader.use(input_texture, resolution);
    const canvas_res = MyEvent.Ret.call("canvas_res");
    const fix_res = resolution.map(n => n * this.#scale);
    const center = canvas_res.map((n, i) => (n - fix_res[i]) / 2);
    const fix_pos = center.map((n, i) => n + this.#position[i]);
    this.#gl.viewport(fix_pos[0], fix_pos[1], fix_res[0], fix_res[1]);
    // this.#gl.scissor(fix_pos[0], fix_pos[1], fix_res[0], fix_res[1]);
    this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, null);
    this.#drawImpl(resolution);
  }
  #drawImpl() {
    this.#gl.bindVertexArray(this.#vao.vao);
    this.#gl.drawElements(this.#gl.TRIANGLES, this.#vao.index_len, this.#gl.UNSIGNED_BYTE, 0);
  }
  readPixels(texture, resolution) {
    this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frame);
    this.#gl.framebufferTexture2D(this.#gl.FRAMEBUFFER, this.#gl.COLOR_ATTACHMENT0, this.#gl.TEXTURE_2D, texture, 0);
    const buf = new Uint8ClampedArray(resolution[0] * resolution[1] * 4);
    this.#gl.readPixels(0, 0, resolution[0], resolution[1], this.#gl.RGBA, this.#gl.UNSIGNED_BYTE, buf);
    return buf;
  }
};