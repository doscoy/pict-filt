class ListItem {
  #next = null;
  #prev = null;
  #item = null;
  constructor(item) { this.#item = item; }
  get next() { return this.#next; }
  get prev() { return this.#prev; }
  get item() { return this.#item; }
  addNext(item) {
    const new_item = new ListItem(item);
    if (this.#next !== null) {
      new_item.#next = this.#next;
      this.#next.#prev = new_item;
    }
    new_item.#prev = this;
    this.#next = new_item;
    return this.#next;
  }
  addPrev(item) {
    const new_item = new ListItem(item);
    if (this.#prev !== null) {
      new_item.#prev = this.#prev;
      this.#prev.#next = new_item;
    }
    new_item.#next = this;
    this.#prev = new_item;
    return this.#prev;
  }
  pop() {
    if (this.#next !== null) {
      this.#next.#prev = this.#prev;
    }
    if (this.#prev !== null) {
      this.#prev.#next = this.#next;
    }
    return this;
  }
  detachPrev() {
    if (this.#prev !== null) {
      this.#prev.#next = null;
    }
    this.#prev = null;
  }
};

export default class List {
  #map = new WeakMap();
  #top = null;
  #bottom = null;
  constructor() { }
  push(item) {
    if (this.#bottom === null) {
      this.#bottom = new ListItem(item);
    } else {
      this.#bottom.addNext(item);
      this.#bottom = this.#bottom.next;
    }
    if (this.#top === null) {
      this.#top = this.#bottom;
    }
    this.#map.set(item, this.#bottom);
  }
  pop(item) {
    if (this.#map.has(item)) {
      const del_list = this.#map.get(item).pop();
      if (del_list === this.#top) {
        this.#top = this.#top.next;
      }
      if (del_list === this.#bottom) {
        this.#bottom = this.#bottom.prev;
      }
      this.#map.delete(item);
      return item;
    }
    return null;
  }
  insertBefore(item, reference) {
    if (this.#map.has(reference)) {
      const ref_list = this.#map.get(reference);
      const new_list = ref_list.addPrev(item);
      if (ref_list === this.#top) {
        this.#top = new_list;
      }
      this.#map.set(item, new_list);
    }
  }
  insertAfter(item, reference) {
    if (this.#map.has(reference)) {
      const ref_list = this.#map.get(reference);
      const new_list = ref_list.addNext(item);
      if (ref_list === this.#bottom) {
        this.#bottom = new_list;
      }
      this.#map.set(item, new_list);
    }
  }
  forEach(func) {
    let buf = this.#top;
    let index = 0;
    while (buf !== null) {
      func(buf.item, index);
      buf = buf.next;
      ++index;
    }
  }
  getArray() {
    const arr = new Array();
    const func = item => arr.push(item);
    this.forEach(func);
    return arr;
  }
  clear() {
    let buf = this.#top;
    while (buf !== null) {
      buf.detachPrev();
      buf = buf.next;
    }
    this.#map = new WeakMap();
    this.#top = null;
    this.#bottom = null;
  }
};