function stopScroll(e) { e.preventDefault(); }

class ValueStepper {
  #value;
  #fix;
  #step;
  constructor(num_str) {
    const buf = typeof (num_str) === "string" ? num_str : String(num_strm);
    this.#value = buf;
    const spl = buf.split(".");
    this.#fix = spl.length <= 1 ? 0 : spl[1].length;
    this.#step = this.#fix === 0 ? 1 : 1.0 / Math.pow(10, spl[1].length);
  }
  get value() { return this.#value; }
  step(step = 1) {
    this.#value = (Number(this.#value) + this.#step * step).toFixed(this.#fix);
    return this.#value;
  }
};

class WheelValue {
  #editor;
  #token_getter;
  #range = null;
  #stepper = null;
  #cursorCallback = (e, s) => this.#cursorCallbackImpl(e, s);
  #wheelCallback = e => this.#wheelCallbackImpl(e);
  constructor(editor, token_getter) {
    this.#editor = editor;
    this.#token_getter = token_getter;
  }
  get cursorCallback() { return this.#cursorCallback; }
  #wheelCallbackImpl(e) {
    const abs = Math.abs(e.wheelY)
    const step = Math.max(1, Math.floor(abs / 15)) * Math.sign(e.wheelY);
    this.#stepper.step(step);
    this.#editor.session.doc.replace(this.#range, this.#stepper.value)
  }
  #cursorCallbackImpl(e, selection) {
    const token = this.#token_getter(selection);
    if (token.data && token.data.type === "constant.numeric") {
      this.#set(token.range);
    } else {
      this.#detach();
    }
  }
  #set(range) {
    if (this.#range !== null) {
      this.#detach();
    }
    document.addEventListener('mousewheel', stopScroll, { passive: false });
    this.#editor.on("mousewheel", this.#wheelCallback);
    let value = this.#editor.session.doc.getTextRange(range);
    if (value[value.length - 1] === ".") {
      value = value.slice(0, value.length - 1);
      --range.end.column;
    }
    this.#range = range;
    this.#stepper = new ValueStepper(value);
  }
  #detach() {
    this.#range = null;
    this.#stepper = null;
    this.#editor.off("mousewheel", this.#wheelCallback);
    document.removeEventListener('mousewheel', stopScroll, { passive: false });
  }
};

ace.define("ace/ext/wheelvalue", ["require", "exports", "module", "ace/editor", "ace/config", "ace/token_iterator", "ace/range"], function (require, exports, module) {
  "use strict";
  const Editor = require("../editor").Editor;
  const TokenIterator = require("../token_iterator").TokenIterator;
  const Range = require("../range").Range;
  const GetToken = function (selection) {
    const pos = selection.getCursor();
    const titr = new TokenIterator(selection.session, pos.row, pos.column);
    const token = titr.getCurrentToken();
    if (token) {
      const row = titr.getCurrentTokenRow();
      const col = titr.getCurrentTokenColumn();
      return { data: token, range: new Range(row, col, row, pos.column), len: pos.column - col };
    } else {
      return { data: token, range: null };
    }
  };

  require("../config").defineOptions(Editor.prototype, "editor", {
    enableWheelValue: {
      set: function (val) {
        if (val) {
          if (!this.wheelValue) {
            this.wheelValue = new WheelValue(this, GetToken);
          }
          this.selection.on("changeCursor", this.wheelValue.cursorCallback);
        } else if (this.wheelValue) {
          this.selection.off("changeCursor", this.wheelValue.cursorCallback);
        }
      },
      value: false
    }
  });
});
(function () {
  ace.require(["ace/ext/wheelvalue"], function (m) {
    if (typeof module == "object" && typeof exports == "object" && module) {
      module.exports = m;
    }
  });
})();