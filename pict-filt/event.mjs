class ReturnEvent {
  #map = new Map();
  add(name, func) { this.#map.set(name, func); }
  remove(name) { this.#map.delete(name); }
  call(name, ...args) {
    if (this.#map.has(name)) {
      return this.#map.get(name)(...args);
    } else {
      return undefined;
    }
  }
};

class MultiEvent {
  #map = new Map();
  add(name, func) {
    if (this.#map.has(name)) {
      this.#map.get(name).add(func);
    } else {
      this.#map.set(name, new Set([func]));
    }
  }
  remove(name, func) {
    if (this.#map.has(name)) {
      const tmp = this.#map.get(name);
      if (tmp.has(func)) {
        tmp.delete(func);
      }
    }
  }
  call(name, ...args) {
    if (this.#map.has(name)) {
      this.#map.get(name).forEach(func => func(...args));
    }
  }
};

export const Ret = new ReturnEvent();
export const Mul = new MultiEvent();